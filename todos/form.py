from todos.models import TodoItem, TodoList
from django import forms


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed"
        ]


class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name"
        ]
